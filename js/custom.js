// Date Picker
  
jQuery(function() {
    jQuery( ".datepicker" ).datepicker();
});

// Click Toggle 
jQuery('.my-booking-form .rooms').change(function () {
    if (jQuery(this).val() === '2') {
         jQuery(".my-booking-form .extra-rooms.r2").show();
         jQuery(".my-booking-form .extra-rooms.r3").hide();
         jQuery(".my-booking-form .extra-rooms.r4").hide();

     }
     else if (jQuery(this).val() === '3') {
         jQuery(".my-booking-form .extra-rooms.r2").show();
         jQuery(".my-booking-form .extra-rooms.r3").show();
          jQuery(".my-booking-form .extra-rooms.r4").hide();

     }
     else if (jQuery(this).val() === '4') {
          jQuery(".my-booking-form .extra-rooms.r2").show();
         jQuery(".my-booking-form .extra-rooms.r3").show();
         jQuery(".my-booking-form .extra-rooms.r4").show();

     }
     else {
         jQuery(".my-booking-form .reservation-form .extra-rooms").hide();
     }
});

// Promo Section

jQuery('.my-booking-form').each(function () {
    var formClick = jQuery(this).find('.promo')
        , formGroup = jQuery(this).find('#add-discount-in');
        formClick.click(function () {
        formClick.toggleClass('addDiscountTextClick')
        formGroup.slideToggle();
    });
});


function reinit(){
    
    // Gallery

    jQuery(document).find('#gallery').find('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.slider-nav',
        dots: false,
        infinite: false,
        arrows: true,
        prevArrow: '<a class="slick-prev"><i class="la la-angle-left"></i></a>',
        nextArrow: '<a class="slick-next"><i class="la la-angle-right"></i></a>'
    });
    jQuery(document).find('#gallery').find('.slider-nav').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        dots: false,
        infinite: false,
        focusOnSelect: true
    });

}

function roomSilderInint() {
    jQuery(document).find('.roomCheck').find('.choose-room-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<a class="slick-prev"><i class="la la-angle-left"></i></a>',
        nextArrow: '<a class="slick-next"><i class="la la-angle-right"></i></a>'
    });
}

jQuery(document).ready(function () { 
    
  jQuery(".my-booking-heading").removeAttr("href");
    
     // Grid Slider
    
    jQuery('.grid-slider').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '<a class="slick-prev"><i class="la la-angle-left"></i></a>',
        nextArrow: '<a class="slick-next"><i class="la la-angle-right"></i></a>'
    });
    
    // Tooltip
    
    jQuery('[data-toggle="tooltip"]').tooltip(); 
    
    // My Booking
    
    jQuery('#booking').each(function () {
        var myBookingForm = jQuery(this).find('.my-booking-form');
        var myBookingInfo = jQuery(this).find('.my-booking-sec');
        var editBtn = jQuery(this).find('.edit');
        myBookingForm.slideUp();
        editBtn.click(function () {
          setTimeout(function(){
            editBtn.parent().parent().toggleClass("bookingFormOpen");
          },300)
            
            myBookingForm.slideToggle();
            myBookingInfo.slideToggle();
        });
    });
    
    // Mobile Booking Form
    
    jQuery('.new-search-wrap a').on('click', function () {
        jQuery('#mobile-form .mobile-form').slideToggle();
    });
    jQuery('.my-booking-form .close-btn').on('click', function () {
        jQuery('#mobile-form .mobile-form').slideToggle();
    });
    jQuery('#mobile-booking-form .edit').on('click', function () {
        jQuery('#mobile-booking-form .open-section').slideToggle();
    });

    // Bp3 Default Radio Section
    
    jQuery('#info-section').each(function () {
        var infoForm = jQuery(this).find('.bp3-form');
        var editBtn = jQuery(this).find('#radio label');
        var closeBtn = jQuery(this).find('#disable1 label');
        infoForm.slideUp();
        editBtn.click(function () {
            infoForm.parent().parent().toggleClass();
            infoForm.slideDown();
        });
        closeBtn.click(function () {
            infoForm.slideUp();
        });
    });
    
    // Bp3 Default Checkbox Section

    jQuery(".guest-detail-element").each(function () {

        var inClick = jQuery(this).find(".form-check-label input[type='checkbox']");

        var enForm = jQuery(this).find(".guest-detail-form");

        inClick.click(function () {
            if (inClick.is(":checked")) {
                enForm.show(300);
            } else {
                enForm.hide(300);
            }
        });

    });
    

function mobileElements() {
    if (jQuery(window).width() < 767) {
        //jQuery(".guest-detail-info .content-sec .checkbox-wrap").detach().appendTo(".checkbox-sec");
        jQuery(".guest-detail-element").each(function () {
          
          var checkInfo = jQuery(this).find(".guest-detail-info");

          var check = jQuery(this).find(".guest-detail-info .checkbox-wrap");
          
          check.detach().appendTo(checkInfo);          

          });
    };
}
  
   jQuery(window).on("load resize", function (e) {
        mobileElements();
    });

    //Button
    
    jQuery('input[type="submit"]').each(function () {
        var myBtn = jQuery(this);
        jQuery(myBtn).click(function () {
            jQuery(this).addClass('loading ');
            setTimeout(function () {
                jQuery(myBtn).removeClass('loading').addClass('done');
            }, 1000);
            setTimeout(function () {
                jQuery(myBtn).removeClass('done');
            }, 1500);
        });
    });
    
    jQuery('.my-booking-heading .edit').on('click', function(){
        jQuery('.my-booking-heading').toggleClass('rotate');
    });
    
    jQuery("#mobile-form .close-btn").click(function(){
        jQuery("#mobile-booking-form").toggleClass("remove");
    });
    jQuery(".new-search-wrap a").click(function(){
        jQuery("#mobile-booking-form").toggleClass("remove");
    });
    jQuery(".navbar-toggler-icon").click(function(){
        jQuery(".navbar-header").toggleClass("mb");
    });
		
});

 jQuery('#roomdetail').on('show.bs.modal', function() {
     setTimeout(function() {
          roomSilderInint();
        }, 150);
})

jQuery(document).on('click','#test',function(){
    setTimeout(function() {
        reinit();
    }, 150);
})

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

jQuery('button.button').click(function () {
    jQuery(this).addClass('loading ');
    setTimeout(function () {
        jQuery('button.button').removeClass('loading').addClass('done');
    }, 1000);
    setTimeout(function () {
        jQuery('button.button').removeClass('done');
    }, 1500);
});